#include <Python.h>

static PyObject* say_hello(PyObject* self, PyObject* args)
{
    const char* name;
    if (!PyArg_ParseTuple(args, "s", &name))
        return NULL;
    printf("Bonjour %s!\n", name);
    Py_RETURN_NONE;
}

static PyMethodDef HelloMethods[] =
{
     {"say_hello", say_hello, METH_VARARGS, "Dit bonjour."},
     NULL
};

static struct PyModuleDef hello_module = {
   PyModuleDef_HEAD_INIT,
   "hello_mod",   NULL,
   -1,   HelloMethods,
   NULL, NULL, NULL, NULL
};

PyMODINIT_FUNC PyInit_hello_mod(void)
{
   return PyModule_Create(&hello_module);
}
