capteur1 = {"fleche", "bord", "arrete"}
capteur2 = {"fleche", "entree"}
capteur3 = {"sortie 2", "entree", "fleche"}

signaux = [capteur1, capteur2, capteur3 ]

if True:
    print("Signaux actifs sur tous les capteurs :")
    sa1 = capteur1
    for capteur in signaux[1:]:
        sa1 = sa1 & capteur
    print(sa1)     # fleche

    print("Signaux actifs au moins un capteur :")
    sa2 = capteur1
    for capteur in signaux[1:]:
        sa2 = sa2 | capteur
    print(sa2)     # fleche, bord, arrete, bord, entree, sortie 2

    print("Signaux actifs sur un nombre impair de capteurs :")
    sa3 = capteur1
    for capteur in signaux[1:]:
        sa3 = sa3 ^ capteur
    print(sa3)     # fleche, bord, arrete, sortie 2

    print("Signaux actifs sur un nombre pair de capteurs :")
    sa4 = sa2 - sa3
    print(sa4) # entree

print("Nombre de fois qu'apparait chaque signal :")
nbsig = {} # clés : str, valeurs : int
for capteur in signaux:
    for signal in capteur:
        if signal in nbsig:
            nbsig[signal] += 1
        else:
            nbsig[signal] = 1
print(nbsig) # entree : 2, fleche: 3...

tout = {
    "signaux": signaux,
    "resultats": {
        "sur tous": sa1,
        "sur au moins un": sa2,
        "sur impair": sa3,
        "sur pair:": sa4,
        "occurences": nbsig   
    }
}
print("Occurences de \entree\" : ", tout["resultats"]["occurences"]["entree"])