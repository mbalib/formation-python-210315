# Application du Design Pattern GoF "Composite"
class Sonde: 
    def nb_sondes(self): 
        return 1
    def v_moyenne(self):
        return self.tot()/self.nb_sondes_simples()
class SondeSimple(Sonde):    
    def __init__(self, v):
        self.v = v
    def afficher(self):
        print(self.v)
    def v_max(self): 
        return self.v        
    def nb_sondes_simples(self): 
        return 1
    def tot(self):
        return self.v
class GroupeDeSondes(Sonde):
    def __init__(self, sondes): # sondes est une liste de Sonde (simples ou groupes)
        self.sondes = sondes
    def afficher(self):
        for s in self.sondes:
            s.afficher()
    def v_max(self):     
        max = -1e10
        # pas optimal en temps :
        for s in self.sondes:
            if max < s.v_max():
                max = s.v_max()
        return max
    def nb_sondes(self):
        tot = 1
        for s in self.sondes:
            tot += s.nb_sondes()
        return tot
    def nb_sondes_simples(self):
        tot = 0
        for s in self.sondes:
            tot += s.nb_sondes_simples()
        return tot        
    def tot(self):
        tot = 0
        for s in self.sondes:
            tot += s.tot()
        return tot
        
s1 = SondeSimple(18)
s2 = SondeSimple(11)
gs1 = GroupeDeSondes([s1, s2])
s3 = SondeSimple(8)
gs2 = GroupeDeSondes([s3, gs1])
gs2.afficher()
# erreur => recursivité infinie : gs1.sondes.append(gs2)
print("s1.v_max() :",s1.v_max()) # 18
print("gs2.v_max() :",gs2.v_max()) # 18
print("gs2.nb_sondes() :",gs2.nb_sondes()) # 5 (3 simples et 2 groupes)
print("gs2.v_moyenne() :",gs2.v_moyenne()) # 12.33