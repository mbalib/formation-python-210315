# Tests
import re

class Correction:
    def mettre_en_minuscules(self, s):
        """Met en minuscules uniquement la chaine
        
        >>> c = Correction()
        >>> c.mettre_en_minuscules("Hello !")
        'hello !'
        >>> s = c.mettre_en_minuscules("ABC")
        >>> s[0]
        'a'
        >>> s[1]
        'b'
        """
        return s.lower()
        
    def enlever_telephones(self, s):
        """Remplace les numéro de tel par des étoiles (i.e. : "redaction")
        >>> c = Correction()
        >>> c.enlever_telephones("Hello !")
        'Hello !'
        >>> c.enlever_telephones("Tom au 0394094, Ann au 07394893")
        'Tom au ***, Ann au ***'
        """
        return re.sub("[0-9]+", "***", s)
        
    def creer_attribut(self):
        """Test dans le cas d'une fonction sans return
        >>> c = Correction()
        >>> c.creer_attribut()
        >>> c.attribut1
        29
        """
        self.attribut1 = 29

if __name__=="__main__":
    import doctest
    doctest.testmod()
    print("Tests finis !")

    