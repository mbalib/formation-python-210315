import datetime
# >>> maintenant = datetime.datetime.now()
# >> > maintenant
# datetime.datetime(2021, 3, 18, 10, 58, 42, 396764)
# >>> prochainferie = datetime.datetime(2021,4,5)
# >>> prochainferie
# datetime.datetime(2021, 4, 5, 0, 0)
# >>> troisjours = datetime.timedelta(days=3)
# >>> prochainferie - troisjours - troisjours
# datetime.datetime(2021, 3, 30, 0, 0)
# >>> troisjours = datetime.timedelta(hours=24*3)
# >>> prochainferie - troisjours - troisjours
# datetime.datetime(2021, 3, 30, 0, 0)
# >>> troisjours
# datetime.timedelta(days=3)
# >>> prochainferie-maintenant
# datetime.timedelta(days=17, seconds=46877, microseconds=603236)
# >>> prochainferie.day
# 5
log = """
2021-03-10 11:43:23 Warning info 1
2021-03-12 21:33:23 Warning info 2
2021-03-15 12:43:23 Error info 3
2021-03-17 11:43:23 Warning info 4
2021-03-18 10:43:23 info info 5
"""
# reafficher le log en remplaçant les dates/heures par "moins d'une heure", "aujourd'hui", "hier"
for ligne in log.splitlines():
    if ligne=="": continue
    y = int(ligne[0:4])
    m = int(ligne[5:7])
    d = int(ligne[8:10])
    h = int(ligne[11:13])
    mi = int(ligne[14:16])
    s = int(ligne[18:20])
    t = datetime.datetime(y,m,d,h,mi,s)
    maintenant = datetime.datetime.now()
    hier = maintenant - datetime.timedelta(days=1)
    if maintenant - t < datetime.timedelta(hours=1):
        ligne = "Moins d'une heure  " + ligne[19:]
    elif maintenant.year == t.year and maintenant.month == t.month and maintenant.day == t.day:
        ligne = "Aujourd'hui        " + ligne[19:]
    elif hier.year == t.year and hier.month == t.month and hier.day == t.day:
        ligne = "Hier               " + ligne[19:]
    print(ligne)